# Russoid – The Russian Arcanoid Game

Russoid is an arcade game that is based on the classic game Arkanoid but with a unique Russian twist. The game features a paddle that is used to bounce a ball and destroy bricks, just like in Arkanoid.

The game has various levels, each with different brick patterns and challenges to overcome. As players progress through the levels, they will encounter power-ups and bonuses that can help them complete the game. Russoid provides a fun and entertaining way to experience Russian culture while enjoying a classic arcade game.

## Authors

- [@arch1varius](https://t.me/arch1varius) [Petukhov Semen, Game Designer]

- [@be1ov](https://t.me/be1ov_v) [Alex Belov, Coder]

- [@kopcenii](https://t.me/kopcenii) [Yaroslav Gusev, 3D Artist]

- [@kur4kk4](https://t.me/kur4kk4) [Ekkert Ilya, QA-Analyst]

## Moodboard

![Moodboard](https://gitlab.com/itmo-ue4/russoid/-/raw/673305e3a52f4376477a28dbe009e183f72f1c4a/docs/MoodBoard.png)

## License

[MIT](https://choosealicense.com/licenses/mit/)
